

--(a) select: Write a query that is equivalent to the following relational algebra expression.
-- σdocid=10398_txt_earn(frequency)     
-- What to turn in: Run your query against your local database and determine the number of records returned as described above. In your browser, turn in a text file, select.txt, which states the number of records.
select count(*) from frequency where docid='10398_txt_earn';

-- (b) select project: Write a SQL statement that is equivalent to the following relational algebra expression.
-- πterm( σdocid=10398_txt_earn and count=1(frequency))
-- What to turn in: Run your query against your local database and determine the number of records returned as described above. In your browser, turn in a text file, select_project.txt, which states the number of records.
select count(term) from frequency where docid='10398_txt_earn' and count=1;

-- (c) union: Write a SQL statement that is equivalent to the following relational algebra expression. (Hint: you can use the UNION keyword in SQL)
-- πterm( σdocid=10398_txt_earn and count=1(frequency)) U πterm( σdocid=925_txt_trade and count=1(frequency))
-- What to turn in: Run your query against your local database and determine the number of records returned as described above. In your browser, turn in a text file, union.txt, which states the number of records.
select count(distinct term) from frequency where (docid='10398_txt_earn' OR docid='925_txt_trade')  and count=1;

-- (d) count: Write a SQL statement to count the number of documents containing the word “parliament”
-- What to turn in: Run your query against your local database and determine the count returned as described above. 
-- In your browser, turn in a text file, count.txt, which states the count.
select count(distinct docid) from frequency where term = "parliament";

-- (e) big documents Write a SQL statement to find all documents that have more than 300 total terms, 
-- including duplicate terms. (Hint: You can use the HAVING clause, or you can use a nested query. 
-- Another hint: Remember that the count column contains the term frequencies, and you want to consider duplicates.) (docid, term_count)
-- What to turn in: Run your query against your local database and determine the number of records returned as described above. 
-- In your browser, turn in a text file, big_documents.txt, which states the number of records.
select count(*) from  (select docid, sum(count) from frequency group by docid having sum(count) > 300) as t;

-- (f) two words: Write a SQL statement to count the number of unique documents that contain both the word 'transactions' and the word 'world'.
-- What to turn in: Run your query against your local database and determine the number of records returned as described above. 
-- In your browser, turn in a text file, two_words.txt, which states the number of records.
select count(f1.docid) from frequency f1, frequency f2 where f1.docid = f2.docid and f1.term = 'transactions' and f2.term = 'world';


-- matrix
-- A(row_num, col_num, value)
-- B(row_num, col_num, value)
select a.row_num, b.col_num, sum(a.value * b.value) from a, b where a.col_num = b.row_num group by a.row_num, b.col_num ;
-- OCTAVE
-- a = [0,0,0,55,78;19,0,21,3,81;0,48,50,1,0;0,0,33,0,67;95,0,0,0,31]
-- b = [0,73,0,0,42;0,0,82,0,0;83,13,0,57,0;48,85,18,24,0;98,7,0,0,3]


-- octave-3.4.0:8> a
-- a =

--     0    0    0   55   78
--    19    0   21    3   81
--     0   48   50    1    0
--     0    0   33    0   67
--    95    0    0    0   31

-- octave-3.4.0:9> b
-- b =

--     0   73    0    0   42
--     0    0   82    0    0
--    83   13    0   57    0
--    48   85   18   24    0
--    98    7    0    0    3

-- octave-3.4.0:10> a*b
-- ans =

--    10284    5221     990    1320     234
--     9825    2482      54    1269    1041
--     4198     735    3954    2874       0
--     9305     898       0    1881     201
--     3038    7152       0       0    4083


-- SIMILIARITY MATRIX = frequecny X transpose(frequency)

-- sqlite> .schema frequency
-- CREATE TABLE Frequency (
-- docid VARCHAR(255),
-- term VARCHAR(255),
-- count int,
-- PRIMARY KEY(docid, term));
select 
f.docid, ft.docid, sum(f.count * ft.count) 
from frequency f, 
frequency ft 
where f.docid < ft.docid 
and f.term = ft.term
group by f.docid, ft.term;
-- ===
select 
f.docid, ft.docid, sum(f.count * ft.count) 
from frequency f, 
frequency ft 
where f.docid = '10080_txt_crude' 
and ft.docid  = '17035_txt_earn'
and f.term = ft.term
group by f.docid ;


-- KEYWORD SEARCH
-- create the view with the data plus the query doc
create view frequency_plus as
SELECT * FROM frequency
UNION SELECT 'q' as docid, 'washington' as term, 1 as count 
UNION SELECT 'q' as docid, 'taxes' as term, 1 as count
UNION SELECT 'q' as docid, 'treasury' as term, 1 as count;
-- get the related document sorted by score desc
select 
f.docid,ft.docid, sum(f.count * ft.count) as score
from frequency_plus f, 
frequency ft 
where f.docid = 'q' 
and f.term = ft.term
group by ft.docid 
order by score desc
limit 1;

--- 





