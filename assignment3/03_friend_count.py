# -*- coding: UTF-8 -*-
import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line


# The input is a 2 element list: [personA, personB]
# personA: Name of a person formatted as a string
# personB: Name of one of personA’s friends formatted as a string
# This implies that personB is a friend of personA, but it does not imply that personA is a friend of personB.

def mapper(record):
    # key: document identifier
    # value: document contents
    p_a = record[0]
    p_b = record[1]
    mr.emit_intermediate(p_a, 1)

# he output should be a (person,  friend count) tuple.
# person is a string and friend count is an integer describing the number of friends ‘person’ has.
# You can test your solution to this problem using friends.json:

# python friend_count.py friends.json
# You can verify your solution against friend_count.json.

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    total = 0
    for v in list_of_values:
      total += v
    mr.emit((key, total))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
