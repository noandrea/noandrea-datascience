import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

# The output should be a joined record.

# The result should be a single list of length 27 
# that contains the fields from the order record followed 
# by the fields from the line item record. 
# Each list element should be a string.

# python join.py records.json

def mapper(record):
    # record[0]: table identifier ['line_item','order'].
    # record[1]:  order_id
    # record[2:]: other values
    order_id = record[1]
    # create an dict for the gable and values
    data = {}
    data['table'] = record[0]
    data['values'] = record
    # emit the data
    mr.emit_intermediate(order_id, data)

def reducer(key, data):
    # key: order_id
    # value: [{'table': line_item | order, 'values': column values}, ...]
    # print "================"+str(key)
    # print data

    orders = []
    lines = []
    for tv in data:
      if tv['table'] == 'order':
        orders.append(tv['values'])
      else:
        lines.append(tv['values'])

    count = 0
    for o in orders:
      for l in lines:
        count += 1
        res = o+l
        mr.emit((res))
    


# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
