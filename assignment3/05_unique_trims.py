import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

# Consider a set of key-value pairs where each 
# key is sequence id and each 
# value is a string of nucleotides, e.g., GCTTCCGAAATGCTCGAA....
# Write a MapReduce query to 
# remove the last 10 characters from each string of nucleotides, 
# then remove any duplicates generated.



# The input is a 2 element list: [sequence id, nucleotides]
# sequence id: Unique identifier formatted as a string
# nucleotides: Sequence of nucleotides formatted as a string

def mapper(record):
    # key: document identifier
    # value: document contents
    seq_id = record[0]
    nucleotides = record[1]
    
    mr.emit_intermediate(nucleotides[:-10], seq_id)


# The output from the reduce function should be the unique trimmed nucleotide strings.
# You can test your solution to this problem using dna.json:
# python unique_trims.py dna.json

# You can verify your solution against unique_trims.json.

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    mr.emit((key))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
