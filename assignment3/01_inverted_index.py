import MapReduce
import sys, json

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    words = value.split()

    w_doc = {}
    for w in words:
      if w not in w_doc:
        w_doc[w] = key

    for w,doc in w_doc.items():
      mr.emit_intermediate(w, doc)

def reducer(key, list_of_values):
    # key: word
    # value: document id 
    docs = []
    for v in list_of_values:
      docs.append(v)
    mr.emit((key, docs))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
