# -*- coding: UTF-8 -*-
import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

# The input is a 2 element list: [personA, personB]
# personA: Name of a person formatted as a string
# personB: Name of one of personA’s friends formatted as a string
# This implies that personB is a friend of personA, but it does not imply that personA is a friend of personB.
# Generate a list of all non-symmetric friend relationships.

def mapper(record):
    # key: document identifier
    # value: document contents
    person = record[0]
    friend = record[1]

    data= {'name': friend, 'type' : 'direct'}
    mr.emit_intermediate(person, data)

    data= {'name': person, 'type' : 'inverse'}
    mr.emit_intermediate(friend, data)

# The output should be a list of (person, friend) and (friend, person) tuples for each asymmetric friendship.
# Only one of the (person, friend) or (friend, person) output tuples will exist in the input. 
# This indicates friendship asymmetry.

# You can test your solution to this problem using friends.json:
# python asymmetric_friendships.py friends.json
# You can verify your solution against asymmetric_friendships.json.

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    person_i_friend = []
    person_friends_me = []

    for v in list_of_values:
      if v['type'] == 'direct':
        person_i_friend.append(v['name'])
      else:
        person_friends_me.append(v['name'])

    for f in person_friends_me:
      if f not in person_i_friend:
        mr.emit((key, f))

    for f in person_i_friend:
      if f not in person_friends_me:
        mr.emit((key, f))        

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
