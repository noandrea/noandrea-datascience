import MapReduce
import sys,json

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line


# Assume you have two matrices 
# A and B in a sparse matrix format, 
# where each record is of the form i, j, value.  
# Design a MapReduce algorithm to compute matrix multiplication: A x B



# The input to the map function will be matrix row records formatted as lists. 
# Each list will have the format 
# [matrix, i, j, value] 
# where matrix is a string and i, j, and value are integers.
# The first item, matrix, is a string that identifies which matrix the record originates from. 
# This field has two possible values:
# 'a' indicates that the record is from matrix A
# 'b' indicates that the record is from matrix B

def mapper(record):
    matrix = record[0]
    i = record[1]
    j = record[2]
    value = record[3]

    m_size = 5
    
    if matrix == 'a':
      for k in range(m_size):
        v = {'m': matrix, 'v':value, 'i':j}
        mr.emit_intermediate((i,k), v)
    else:
      for k in range(m_size):
        v = {'m': matrix, 'v':value, 'i':i}
        mr.emit_intermediate((k,j), v)
        



# The output from the reduce function will also be matrix row records formatted as tuples. 
# Each tuple will have the format (i, j, value) where each element is an integer.

# You can test your solution to this problem using matrix.json:
# python multiply.py matrix.json
# You can verify your solution against multiply.json.

def reducer(key, list_of_values):
    
    m_size = 5

    mult = {'a' : [0]*m_size, 'b':[0]*m_size}

    for v in list_of_values:
      matrix = v['m']
      index = v['i']
      value = v['v']
      mult[matrix][index] = value
    
    res = 0
    for k in range(m_size):
      res += mult['b'][k] * mult['a'][k]  
    
    mr.emit((key[0],key[1],res))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
