import sys,re,string,json

def hw():
	print 'Hello, world!'

def lines(fp):
	print str(len(fp.readlines()))

def loadIndex(sent_file):
	scores = {} # initialize an empty dictionary
	for line in sent_file:
		term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
		scores[term] = int(score)  # Convert the score to an integer.
	# print scores.items() # Print every (term, score) pair in the dictionary
	return scores


def main():
	sent_file = open(sys.argv[1])
	tweet_file = open(sys.argv[2])
	#hw()
	#lines(sent_file)
	#lines(tweet_file)

	scores = loadIndex(sent_file)

	punct = re.compile('[%s]' % re.escape(string.punctuation))
	whitespaces = re.compile('\s+')

	for line in tweet_file:
		tweet = json.loads(line)

		try:
			text = tweet['text'].lower()

			# remove punctuation 
			text = punct.sub(' ', text)
			# prepare score 
			tscores = 0
			# split line 
			for wd in whitespaces.split(text):
				if wd in scores:
					tscores += scores[wd]
			print float(tscores)
		except Exception, e:
			error =  'invalid tweet'






	



if __name__ == '__main__':
    main()
