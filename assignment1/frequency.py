import sys,re,string,json

def hw():
	print 'Hello, world!'

def lines(fp):
	print str(len(fp.readlines()))


def main():
	tweet_file = open(sys.argv[1])
	#hw()
	#lines(sent_file)
	#lines(tweet_file)

	punct = re.compile('[%s]' % re.escape(string.punctuation))
	whitespaces = re.compile('\s+')

	# dictionary of words without score
	word_frequencies = {}

	for line in tweet_file:
		tweet = json.loads(line)

		try:
			text = tweet['text'].lower()

			# remove punctuation 
			text = punct.sub(' ', text)
			# split line 
			for wd in whitespaces.split(text):
				wd = wd.strip()
				if len(wd) == 0:
					continue
				if wd in word_frequencies:
					word_frequencies[wd] += 1
				else: 
					word_frequencies[wd] = 1

		except Exception, e:
			error =  'invalid tweet'
	total_frequencies = float(sum(word_frequencies.values()))
	for wd in word_frequencies:
		print '%s %f' % ( wd,  (float(word_frequencies[wd]) / total_frequencies))

if __name__ == '__main__':
	main()
