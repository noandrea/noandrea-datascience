import sys,re,string,json,operator

def hw():
	print 'Hello, world!'

def lines(fp):
	print str(len(fp.readlines()))

def loadIndex(sent_file):
	scores = {} # initialize an empty dictionary
	for line in sent_file:
		term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
		scores[term] = int(score)  # Convert the score to an integer.
	# print scores.items() # Print every (term, score) pair in the dictionary
	return scores

def main():
	sent_file = open(sys.argv[1])
	tweet_file = open(sys.argv[2])
	#hw()
	#lines(sent_file)
	#lines(tweet_file)

	scores = loadIndex(sent_file)

	punct = re.compile('[%s]' % re.escape(string.punctuation))
	whitespaces = re.compile('\s+')

	# dictionary of words without score
	ht_frequencies = {}

	countries_happiness = {}

	for line in tweet_file:
		tweet = json.loads(line)

		try:

			country = tweet['place']['country_code']
			if country != 'US':
				continue
			
			place_type = tweet['place']['place_type']
			if place_type != 'city':
				continue

			fullname = tweet['place']['full_name']
			us_country = fullname.split(', ')[-1]
			countries_happiness[us_country] = 0

			# get the text
			text = tweet['text'].lower()
			# remove punctuation 
			text = punct.sub(' ', text)
			# prepare score 
			tscores = float(0)
			# split line 
			for wd in whitespaces.split(text):
				if wd in scores:
					tscores += float(scores[wd])
			countries_happiness[us_country] += tscores


		except Exception, e:
			error =  'invalid tweet'
	
	happiest =  sorted(countries_happiness.iteritems(), key=operator.itemgetter(1), reverse=True)
	if len(happiest) > 0:
		k,v = happiest[0]
		print k

if __name__ == '__main__':
	main()
