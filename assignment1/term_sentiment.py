import sys,re,string,json

def hw():
	print 'Hello, world!'

def lines(fp):
	print str(len(fp.readlines()))

def loadIndex(sent_file):
	scores = {} # initialize an empty dictionary
	for line in sent_file:
		term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
		scores[term] = int(score)  # Convert the score to an integer.
	# print scores.items() # Print every (term, score) pair in the dictionary
	return scores

def main():
	sent_file = open(sys.argv[1])
	tweet_file = open(sys.argv[2])
	#hw()
	#lines(sent_file)
	#lines(tweet_file)

	scores = loadIndex(sent_file)

	punct = re.compile('[%s]' % re.escape(string.punctuation))
	whitespaces = re.compile('\s+')

	# dictionary of words without score
	score_estimates = {}

	for line in tweet_file:
		tweet = json.loads(line)

		try:
			text = tweet['text'].lower()

			# remove punctuation 
			text = punct.sub(' ', text)
			# prepare score 
			tscores = 0
			# split line 
			not_found = []
			for wd in whitespaces.split(text):
				if wd.strip() == '':
					continue

				if wd in scores:
					tscores += scores[wd]
				else: 
					not_found.append(wd)

			for wd in not_found:
				if wd not in score_estimates:
					score_estimates[wd] = tscores
				else:
					score_estimates[wd] += tscores

		except Exception, e:
			error =  'invalid tweet'
	for k,v in score_estimates.items():
		print '%s %f' % (k, float(v))

if __name__ == '__main__':
	main()
