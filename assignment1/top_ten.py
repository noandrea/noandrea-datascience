import sys,re,string,json,operator

def hw():
	print 'Hello, world!'

def lines(fp):
	print str(len(fp.readlines()))


def main():
	tweet_file = open(sys.argv[1])
	#hw()
	#lines(sent_file)
	#lines(tweet_file)

	punct = re.compile('[%s]' % re.escape(string.punctuation))
	whitespaces = re.compile('\s+')

	# dictionary of words without score
	ht_frequencies = {}

	for line in tweet_file:
		tweet = json.loads(line)

		try:
			# print json.dumps(tweet,indent=2)
			hashtags = tweet['entities']['hashtags']

			for wd in hashtags:
				wd = wd['text'].lower()
				if wd in ht_frequencies:
					ht_frequencies[wd] += 1
				else: 
					ht_frequencies[wd] = 1

		except Exception, e:
			error = 'invalid tweet'

	# total frequencies
	total_frequencies = float(sum(ht_frequencies.values()))

	results = {}

	for wd in ht_frequencies:
		results[wd] = (float(ht_frequencies[wd]) / total_frequencies)
	
	for k,v in sorted(ht_frequencies.iteritems(), key=operator.itemgetter(1))[-10:]:
		print '%s %f' % (k,v)

if __name__ == '__main__':
	main()
